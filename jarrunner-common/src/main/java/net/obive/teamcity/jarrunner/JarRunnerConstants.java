package net.obive.teamcity.jarrunner;

import org.jetbrains.annotations.NonNls;

public class JarRunnerConstants {
  // After updating config verson please also update settings converter (in RakeRunnerRunType)
  // and update config verson in settings edit ui (taskRunnerRunParams.jsp). Last one is needed for correct conversion
  // if user copied old-style build configuration and then updated some settings in it.
  @NonNls public static final  String CURRENT_CONFIG_VERSION = "1";
  @NonNls public static final String SERVER_CONFIGURATION_VERSION_PROPERTY = "ui.jarRunner.config.version";

  @NonNls public static final String RUNNER_TYPE = "jar-runner";


//  @NonNls public static final String SERVER_UI_JVM_VMOPTIONS = "ui.jarRunner.jvm.vmoptions";
  @NonNls public static final String SERVER_UI_JVM_CLASS_PATH = "ui.jarRunner.jvm.classpath";
  @NonNls public static final String SERVER_UI_MAIN_CLASS = "ui.jarRunner.mainClass";
  @NonNls public static final String SERVER_UI_ENV_FILE = "ui.jarRunner.envFile";
  @NonNls public static final String SERVER_UI_CONFIG = "ui.jarRunner.config";
  @NonNls public static final String SERVER_UI_CONFIG_REPLACED = "ui.jarRunner.configReplaced";
//  @NonNls public static final String SERVER_UI_ARGUMENTS = "ui.jarRunner.arguments";

  @NonNls public static final String AGENT_BUNDLE_JAR = "jar-runner.jar";


}
