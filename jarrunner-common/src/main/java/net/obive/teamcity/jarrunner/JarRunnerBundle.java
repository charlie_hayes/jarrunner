package net.obive.teamcity.jarrunner;

public class JarRunnerBundle {
	static String DEFAULT_RVM_SDK = "system";

	static String RUNNER_DESCRIPTION = "Runner for executing JARs";
	static String RUNNER_DISPLAY_NAME = "JAR Runner";

	static String RUNNER_ERROR_TITLE_PROBLEMS_IN_CONF_ON_AGENT = "Failed to run Rake..";
	static String RUNNER_ERROR_TITLE_JRUBY_PROBLEMS_IN_CONF_ON_AGENT = "Failed to run JRuby..";
	static String MSG_OS_NOT_SUPPORTED = "OS isn't supported!";
}
