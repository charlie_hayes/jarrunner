<%@ taglib prefix="props" tagdir="/WEB-INF/tags/props" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--<jsp:useBean id="propertiesBean" scope="request" type="jetbrains.buildServer.controllers.BasePropertiesBean"/>--%>

<%@include file="globalConsts.jsp" %>
<%@include file="jarRunnerConsts.jsp" %>

<div class="parameter">
  JAR File:
  <props:displayValue name="${BUILD_FILE_PATH_KEY}" emptyValue="not specified"/>
</div>

<props:viewWorkingDirectory/>

<div class="parameter">

  <div class="nestedParameter">Classpath: <strong><props:displayValue
      name="${UI_JVM_CLASS_PATH}" emptyValue="not specified"/></strong></div>

    <div class="nestedParameter">Main Class: <strong><props:displayValue
      name="${UI_MAIN_CLASS}" emptyValue="not specified"/></strong></div>


</div>

