<%@ page import="net.obive.teamcity.jarrunner.JarRunnerConstants" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--<c:set var="UI_JVM_VMOPTIONS" value="<%=JarRunnerConstants.SERVER_UI_JVM_VMOPTIONS%>"/>--%>
<c:set var="UI_JVM_CLASS_PATH" value="<%=JarRunnerConstants.SERVER_UI_JVM_CLASS_PATH%>"/>
<%--<c:set var="UI_ARGUMENTS" value="<%=JarRunnerConstants.SERVER_UI_ARGUMENTS%>"/>--%>
<c:set var="UI_MAIN_CLASS" value="<%=JarRunnerConstants.SERVER_UI_MAIN_CLASS%>"/>
<c:set var="UI_ENV_FILE" value="<%=JarRunnerConstants.SERVER_UI_ENV_FILE%>"/>
<c:set var="UI_CONFIG" value="<%=JarRunnerConstants.SERVER_UI_CONFIG%>"/>



<c:set var="CONFIGURATION_VERSION_PROPERTY" value="<%=JarRunnerConstants.SERVER_CONFIGURATION_VERSION_PROPERTY%>"/>
<c:set var="CONFIGURATION_VERSION_CURRENT" value="<%=JarRunnerConstants.CURRENT_CONFIG_VERSION%>"/>

