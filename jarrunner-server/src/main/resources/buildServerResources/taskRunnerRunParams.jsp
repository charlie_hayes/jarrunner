<%@ taglib prefix="props" tagdir="/WEB-INF/tags/props" %>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="l" tagdir="/WEB-INF/tags/layout" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<%@include file="globalConsts.jsp" %>
<%@include file="jarRunnerConsts.jsp" %>

<style>
	pre.smallNote {
		margin-left: 0;
		font-size: 70%;
	}

	.warning {
		color: orange;
	}
</style>

<%--Default initial settings format version--%>
<props:hiddenProperty name="${CONFIGURATION_VERSION_PROPERTY}" value="${CONFIGURATION_VERSION_CURRENT}"/>

<%-- for debugging --%>
<%--<% request.setAttribute("artifactBuild", net.obive.teamcity.jarrunner.JarRunnerRunType.getArtifactBuild(buildData)); %>--%>
<%--requestScope <ul><c:forEach items='${requestScope}' var='p'><li><b><c:out value='${p.key}'/></b> <c:out value='${p.value}'/></li></c:forEach></ul>
pageScope <ul><c:forEach items='${pageScope}' var='p'><li><b><c:out value='${p.key}'/></b> <c:out value='${p.value}'/></li></c:forEach></ul>
sessionScope <ul><c:forEach items='${sessionScope}' var='p'><li><b><c:out value='${p.key}'/></b> <c:out value='${p.value}'/></li></c:forEach></ul>
applicationScope <ul><c:forEach items='${applicationScope}' var='p'><li><b><c:out value='${p.key}'/></b> <c:out value='${p.value}'/></li></c:forEach></ul>--%>

<l:settingsGroup title="Class information">

	<%-- Bogus build file is inteionally preloaded in net/obive/teamcity/jarrunner/JarRunnerRunType.java --%>
	<%--
		<tr>
			<th><label for="${BUILD_FILE_PATH_KEY}">Path to a JAR file:</label></th>
			<td>
				<props:textProperty name="${BUILD_FILE_PATH_KEY}" className="longField">
					<jsp:attribute name="afterTextField"><bs:vcsTree fieldId="${BUILD_FILE_PATH_KEY}"/></jsp:attribute>
				</props:textProperty>
				<span class="error" id="error_${BUILD_FILE_PATH_KEY}"></span>
				<span class="smallNote">Enter JAR file path; Specified path should be relative to the checkout directory.</span>
			</td>
		</tr>
	--%>

	<tr id="${UI_JVM_CLASS_PATH}_scope">
		<th><label for="${UI_JVM_CLASS_PATH}">Classpath: </label></th>
		<td>
				<%-- TODO add additional checks for possible classpath sources like previous build runners or peek into the artifact directory --%>
				<%-- Show warning if there are no enabled artifact dependencies --%>
			<s:eval expression="T(net.obive.teamcity.Util).getEnabledDependencies(buildForm.settingsBuildType)"
					var="enabledDeps"/>
			<c:if test="${fn:length(enabledDeps) == 0}">
				<div class="warning">
					<c:choose>
						<c:when test="${fn:length(buildForm.settingsBuildType.artifactDependencies) > 0}">
							All artifact dependencies are disabled<br>
						</c:when>
						<c:otherwise>
							You probably want at least one enabled artifact dependency.<br>
						</c:otherwise>
					</c:choose>
					Most likely the main class won't be found since there are no enabled artifact dependencies.
				</div>
			</c:if>

				<%-- These are a few elements the built-in TeamCity JS expects to build the artifact tree --%>
			<select name="revisionRules" id="revisionRules" style="display:none;">
				<option value="lastSuccessful"></option>
				<option value="lastPinned"></option>
				<option value="lastFinished"></option>
				<option value="sameChainOrLastFinished" selected></option>
				<option value="buildNumber"></option>
				<option value="buildTag"></option>
			</select>
			<input type="text" name="buildNumberPattern" id="buildNumberPattern" value="" style="display:none;"
				   disabled>
			<input type="hidden" id="sourceBuildTypeId"
				   value="${buildForm.settingsBuildType.lastChangesFinished.buildTypeId}"/>


				<%-- Classpath field --%>
			<props:multilineProperty name="${UI_JVM_CLASS_PATH}" className="buildTypeParams longField" expanded="true"
									 linkTitle="" cols="40" rows="6">
                <jsp:attribute name="afterTextField">
                    <i id="${UI_JVM_CLASS_PATH}_insertArtifact" class="tc-icon icon16 tc-icon_folders agentTree"
					   title="Select files from the latest build"
					   showdiscardchangesmessage="false"
					   onclick="return BS.ArtifactsTreePopup_artifactPaths.showPopup(this);"></i>
                </jsp:attribute>
			</props:multilineProperty>

			<script type="text/javascript">
				var buildTypeId = "<c:out value="${buildForm.settingsBuildType.lastChangesFinished.buildTypeExternalId}" />";

				BS.ArtifactsTreePopup_artifactPaths = {
					showPopup: function (elem) {
						var popup = new BS.Popup("editArtifactsTreePopup", {
							hideOnMouseOut: false,
							hideOnMouseClickOutside: true,
							shift: {x: 20, y: 0},
							url: window["base_uri"] + "/editArtifactsTreePopup.html?buildTypeId=" + buildTypeId
						});
						popup.showPopupNearElement(elem);

						this.prepareSelection($j(elem).closest(".posRel").find(".__popupAttached").attr("id"));

						return false;
					},

					prepareSelection: function (id) {
						var textarea = $j('[id="' + id + '"]');
						$j(window).off("bs.agentFile bs.agentDir").on("bs.agentFile bs.agentDir", function (e, path) {

							var pathToAppend = path;
							var value = textarea.val();
							if (value.length > 0) {
								var lines = value.split("\n");
								lines.push(pathToAppend);
								textarea.val(lines.join("\n"));
							} else {
								textarea.val(pathToAppend);
							}
							BS.VisibilityHandlers.updateVisibility(id);
							BS.EditBuildTypeForm.setModified(true);   // In theory this can be wrong, but in 99% of cases the form is modified.
						});
					}
				};

				// The HTML TeamCity returns has # hrefs on all the links which no JS cancels. Let's neutralize those so the page doesn't scroll
				setTimeout(function () {
					$j('[id="${UI_JVM_CLASS_PATH}_insertArtifact"]').on('click', function () {
						var found = false;
						var interval = setInterval(function () {
							var $popup = $j('#editArtifactsTreePopup');
							if ($popup.length === 0) {
								if (found)
									clearInterval(interval);
							} else {
								found = true;
								$popup.find('a').attr('href', 'javascript:void(0);');
							}
						}, 10);
					});
				}, 100);

			</script>
			<span class="smallNote">Class path entries separated by newlines <br/>Use wildcard paths like &apos;lib/*&apos;</span>
		</td>
	</tr>

	<tr>
		<th><label for="${UI_MAIN_CLASS}">Main Class: </label></th>
		<td>
			<props:textProperty name="${UI_MAIN_CLASS}" className="longField"/>
			<span class="smallNote">Main class to run, <a href="javascript:void(0)"
														  onclick="$j('#main-class-more-info').toggle()">more info...</a></span>
			<div id="main-class-more-info" style="display: none;">
				<h4>Class must implement <span class="mono">Runnable</span>:</h4>
				<pre class="smallNote">
import jetbrains.buildServer.agent.AgentRunningBuild;
import jetbrains.buildServer.agent.BuildRunnerContext;

public class MyBuildStep implements Runnable {

  // Use constructor signature to get access to TeamCity objects; useful for logging
  public MyBuildStep(AgentRunningBuild agentRunningBuild, BuildRunnerContext context, Consumer&lt;BuildFinishedStatus&gt; buildFinishedStatusSetter){

  }
  public void run(){

  }
}</pre>


				<h4>Add to POM for dependencies:</h4>
				<pre class="smallNote">
&lt;repositories&gt;
  &lt;repository&gt;
    &lt;id&gt;JetBrains&lt;/id&gt;
    &lt;url&gt;http://repository.jetbrains.com/all&lt;/url&gt;
  &lt;/repository&gt;
&lt;/repositories&gt;

&lt;dependencies&gt;
  &lt;dependency&gt;
    &lt;groupId&gt;org.jetbrains.teamcity&lt;/groupId&gt;
    &lt;artifactId&gt;agent-api&lt;/artifactId&gt;
    &lt;version&gt;10.0&lt;/version&gt;
    &lt;scope&gt;provided&lt;/scope&gt;
  &lt;/dependency&gt;
&lt;/dependencies&gt;</pre>
			</div>
		</td>
	</tr>
	<tr>
		<th><label for="${UI_ENV_FILE}">Env File: </label></th>
		<td>
			<props:multilineProperty name="${UI_ENV_FILE}" className="buildTypeParams longField" expanded="true"
									 linkTitle="" cols="40" rows="1">
                <jsp:attribute name="afterTextField">
                    <i id="${UI_JVM_CLASS_PATH}_insertArtifact" class="tc-icon icon16 tc-icon_folders agentTree"
					   title="Select files from the latest build"
					   showdiscardchangesmessage="false"
					   onclick="return BS.ArtifactsTreePopup_artifactPaths.showPopup(this);"></i>
                </jsp:attribute>
			</props:multilineProperty>

			<span class="smallNote">Env file for config env variables (optional), <a href="javascript:void(0)"
																					 onclick="$j('#env-file-more-info').toggle()">more info...</a></span>
			<div id="env-file-more-info" style="display: none;">
				<h4>Sample file:</h4>
				<pre class="smallNote">
VAR_ONE=value
VAR_TWO=value2</pre>
				<h4>Sample config:</h4>
				<pre class="smallNote">
{VAR_ONE},{VAR_TWO}</pre>

			</div>
		</td>
	</tr>
	<tr>
		<th><label for="${UI_CONFIG}">Config: </label></th>
		<td>
			<props:multilineProperty name="${UI_CONFIG}" className="buildTypeParams longField" expanded="true"
									 linkTitle="" cols="40" rows="6"/>
			<p>This is just a text blob made available to the Main Class through the <b>context</b> argument.</p>
			<pre class="smallNote">
String config = context.getRunnerParameters().get("ui.jarRunner.config");
String configEnvVarsReplaced = context.getRunnerParameters().get("ui.jarRunner.configReplaced");</pre>
			<p>Also consider using <b>Parameters</b> for the build config.<br/> Load them from the Main Class using the
				<b>context</b> argument.</p>
			<pre class="smallNote">
Map&lt;String, String&gt; params = context.getBuildParameters().getAllParameters();
String in = params.get("system.overlayIn");</pre>
		</td>
	</tr>

</l:settingsGroup>

