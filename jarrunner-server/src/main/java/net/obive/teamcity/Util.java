package net.obive.teamcity;

import jetbrains.buildServer.serverSide.SBuildType;
import jetbrains.buildServer.serverSide.artifacts.SArtifactDependency;

import java.util.List;
import java.util.stream.Collectors;


public class Util {


	public static List<SArtifactDependency> getEnabledDependencies(final SBuildType buildType) {
		List<SArtifactDependency> result;
		if (buildType == null) {
			result = null;
		} else {
			result = buildType.getArtifactDependencies().stream().filter(d -> buildType.isEnabled(d.getId())).collect(Collectors.toList());
		}
		return result;
	}
}
