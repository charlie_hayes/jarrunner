package net.obive.teamcity.jarrunner;


import jetbrains.buildServer.serverSide.InvalidProperty;
import jetbrains.buildServer.serverSide.PropertiesProcessor;
import jetbrains.buildServer.serverSide.RunType;
import jetbrains.buildServer.serverSide.RunTypeRegistry;
import jetbrains.buildServer.web.openapi.PluginDescriptor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static jetbrains.buildServer.runner.BuildFileRunnerConstants.BUILD_FILE_KEY;
import static jetbrains.buildServer.runner.BuildFileRunnerConstants.USE_CUSTOM_BUILD_FILE_KEY;

public class JarRunnerRunType extends RunType {


	@NotNull
	private final PluginDescriptor descriptor;
	public JarRunnerRunType(@NotNull final RunTypeRegistry runTypeRegistry, @NotNull PluginDescriptor descriptor) {
		this.descriptor = descriptor;
		runTypeRegistry.registerRunType(this);
	}

	@Override
	@Nullable
	public PropertiesProcessor getRunnerPropertiesProcessor() {
		return new ParametersValidator();
	}

	@Override
	public String getEditRunnerParamsJspFilePath() {
		return descriptor.getPluginResourcesPath("taskRunnerRunParams.jsp");
	}

	@Override
	public String getViewRunnerParamsJspFilePath() {
		return descriptor.getPluginResourcesPath("viewTaskRunnerRunParams.jsp");
	}


	@Nullable
	public Map<String, String> getDefaultRunnerProperties() {
		final Map<String, String> map = new HashMap<String, String>();


		// configuration version
		map.put(JarRunnerConstants.SERVER_CONFIGURATION_VERSION_PROPERTY,
				JarRunnerConstants.CURRENT_CONFIG_VERSION);

		map.put(USE_CUSTOM_BUILD_FILE_KEY, Boolean.TRUE.toString());

		// cannot be empty or null since TC checks internally, see common-api-10.0-sources.jar!/jetbrains/buildServer/runner/BuildFileRunnerUtil.java
		map.put(BUILD_FILE_KEY, "no build file needed");


		return map;
	}


	@NotNull
	@Override
	public String getDescription() {
		return JarRunnerBundle.RUNNER_DESCRIPTION;
	}

	@NotNull
	@Override
	public String getDisplayName() {
		return JarRunnerBundle.RUNNER_DISPLAY_NAME;
	}

	@NotNull
	@Override
	public String getType() {
		return JarRunnerConstants.RUNNER_TYPE;
	}

	static class ParametersValidator implements PropertiesProcessor {

		public Collection<InvalidProperty> process(final Map<String, String> properties) {
			final Collection<InvalidProperty> ret = new ArrayList<InvalidProperty>(1);

//			ret.add(new InvalidProperty(RakeRunnerConstants.SERVER_UI_RUBY_RVM_SDK_NAME, "The interpeter name must be specified."));

			return ret;
		}
	}
}
