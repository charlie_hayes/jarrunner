package net.obive.teamcity.jarrunner;

import jetbrains.buildServer.controllers.ActionErrors;
import jetbrains.buildServer.controllers.StatefulObject;
import jetbrains.buildServer.controllers.admin.projects.BuildTypeForm;
import jetbrains.buildServer.controllers.admin.projects.EditRunTypeControllerExtension;
import jetbrains.buildServer.serverSide.BuildTypeSettings;
import jetbrains.buildServer.serverSide.SBuildServer;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public class EditJarRunnerRunType implements EditRunTypeControllerExtension {

	public EditJarRunnerRunType(@NotNull final SBuildServer server) {
		server.registerExtension(EditRunTypeControllerExtension.class, JarRunnerConstants.RUNNER_TYPE, this);
	}

	public void fillModel(@NotNull HttpServletRequest request, @NotNull BuildTypeForm form, @NotNull Map model) {

	}
	public void updateState(@NotNull HttpServletRequest request, @NotNull BuildTypeForm form) {

	}
	@Nullable
	public StatefulObject getState(@NotNull HttpServletRequest request, @NotNull BuildTypeForm form) {
		return null;
	}
	@NotNull
	public ActionErrors validate(@NotNull HttpServletRequest request, @NotNull BuildTypeForm form) {
		return new ActionErrors();
	}
	public void updateBuildType(@NotNull HttpServletRequest request, @NotNull BuildTypeForm form, @NotNull BuildTypeSettings buildTypeSettings, @NotNull ActionErrors errors) {

	}
}
