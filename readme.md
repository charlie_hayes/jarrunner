# JAR Runner TeamCity plugin

This plugin essentially lets you write TeamCity plugins in your code repo and then run them as a build step.

The magic comes from being able to log using the Java API instead of the specially formatted standard out messages.

Although you can use JAR's for the classpath of your build step, and despite the name, it doesn't actually have to be in a JAR

## Build
Issue 'mvn clean compile package' command from the root project to build the plugin. Resulting package <artifactId>.zip will be placed in 'target' directory.
 
## Install
To install the plugin, put zip archive into 'plugins' dir under TeamCity data directory and restart the server.

## Writing a Class to run with JAR Runner
### Easiest but least useful option
1. Write a class that Implement `Runnable`
2. Setup TeamCity to build this class and produce an artifact
3. Add a JAR Runner build step
4. Add the class path to the JAR Runner config
5. Specify the class created in step 1 as the main class in the JAR Runner config
6. Save and run build! 

### Most useful option

1. Add a maven reference to the TeamCity lib

		<project>
			<repositories>
				<repository>
					<id>JetBrains</id>
					<url>http://repository.jetbrains.com/all</url>
				</repository>
			</repositories>
			
			<dependencies>
				<dependency>
					<groupId>org.jetbrains.teamcity</groupId>
					<artifactId>agent-api</artifactId>
					<version>10.0</version>
					<scope>provided</scope>
				</dependency>
			</dependencies>
		</project>
		
2. Create a class following this template:

		import jetbrains.buildServer.agent.AgentRunningBuild;
		import jetbrains.buildServer.agent.BuildFinishedStatus;
		import jetbrains.buildServer.agent.BuildProgressLogger;
		import jetbrains.buildServer.agent.BuildRunnerContext;
		
		public class MyBuildStep implements Runnable {
		
			private final BuildProgressLogger buildLogger;
			private final BuildRunnerContext context;
			private final Consumer<BuildFinishedStatus> finishedStatusSetter;
			
			public MyBuildStep(AgentRunningBuild agentRunningBuild, BuildRunnerContext context, Consumer<BuildFinishedStatus> finishedStatusSetter){
				this.buildLogger = agentRunningBuild.getBuildLogger();
				this.context = context;
				this.finishedStatusSetter = finishedStatusSetter;
			}
			public void run(){
				buildLogger.message("Hey! Look! I'm doing work!");
				finishedStatusSetter.accept(BuildFinishedStatus.FINISHED_WITH_PROBLEMS);
			}
		}

3. Setup TeamCity to build this class and produce an artifact
4. Add a JAR Runner build step
5. Add the class path to the JAR Runner config
6. Specify the class created in step 2 as the main class in the JAR Runner config
7. Save and run build

## Configuration Options
### Env file
Variables used for replacement in the config parameter below. Values can reference other variables; Apache StringSubstitutor will be used to collapse.
#### Sample file:
```
VAR_ONE=value
VAR_TWO=value2
COMBINED_VARS={VAR_ONE}{VAR_TWO}
```
#### Sample config:
```
{VAR_ONE},{VAR_TWO},{COMBINED_VARS}
```

### Config
This is just a text blob made available to the Main Class through the context argument. The replaced version will use variables from the Env File above.
```
String config = context.getRunnerParameters().get("ui.jarRunner.config");
String configEnvVarsReplaced = context.getRunnerParameters().get("ui.jarRunner.configReplaced");
```
As an alternitive to this option, consider using Parameters for the build config.
Load them from the Main Class using the context argument.
```
Map<String, String> params = context.getBuildParameters().getAllParameters();
String in = params.get("system.overlayIn");
```