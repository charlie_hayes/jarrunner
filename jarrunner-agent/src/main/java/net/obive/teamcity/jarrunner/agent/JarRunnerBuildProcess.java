package net.obive.teamcity.jarrunner.agent;

import jetbrains.buildServer.RunBuildException;
import jetbrains.buildServer.agent.AgentRunningBuild;
import jetbrains.buildServer.agent.BuildFinishedStatus;
import jetbrains.buildServer.agent.BuildProcess;
import jetbrains.buildServer.agent.BuildProgressLogger;
import jetbrains.buildServer.agent.BuildRunnerContext;
import org.jetbrains.annotations.NotNull;

class JarRunnerBuildProcess implements BuildProcess {

	private final AgentRunningBuild agentRunningBuild;
	private final BuildRunnerContext context;
	JarRunnerBuildTask jarRunnerBuildTask;
	private boolean finished;
	private BuildFinishedStatus finishedStatus;
	private Thread buildThread;
	BuildProgressLogger buildLogger;


	public JarRunnerBuildProcess(AgentRunningBuild agentRunningBuild, BuildRunnerContext context) {
		this.agentRunningBuild = agentRunningBuild;
		this.context = context;
		this.jarRunnerBuildTask = null;
		this.finished = false;
		this.finishedStatus = BuildFinishedStatus.FINISHED_FAILED;
		this.buildLogger = agentRunningBuild.getBuildLogger();
	}
	public void start() throws RunBuildException {


		buildLogger.message("Starting JAR Runner");

		jarRunnerBuildTask = new JarRunnerBuildTask(this, agentRunningBuild, context);

		buildThread = new Thread(jarRunnerBuildTask);
		buildThread.start();
	}
	public boolean isInterrupted() {
		return buildThread.isInterrupted();
	}
	public boolean isFinished() {
		return finished;
	}
	public void interrupt() {
		if (buildThread != null && !buildThread.isInterrupted()) {
			buildThread.interrupt();
		}
	}
	@NotNull
	public BuildFinishedStatus waitFor() throws RunBuildException {
		try {
			buildThread.join();
			if (buildThread.isInterrupted())
				finishedStatus = BuildFinishedStatus.INTERRUPTED;
		} catch (InterruptedException e) {
			buildLogger.exception(e);
			finishedStatus = BuildFinishedStatus.INTERRUPTED;
		}
		return finishedStatus;
	}

	public void setFinished(boolean finished) {
		this.finished = finished;
	}
	public void setFinishedStatus(BuildFinishedStatus finishedStatus) {
		this.finishedStatus = finishedStatus;
		this.finished = true;
	}
}
