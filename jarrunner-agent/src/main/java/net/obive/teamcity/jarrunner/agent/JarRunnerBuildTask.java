package net.obive.teamcity.jarrunner.agent;

import jetbrains.buildServer.BuildProblemData;
import jetbrains.buildServer.agent.AgentRunningBuild;
import jetbrains.buildServer.agent.BuildFinishedStatus;
import jetbrains.buildServer.agent.BuildProgressLogger;
import jetbrains.buildServer.agent.BuildRunnerContext;
import jetbrains.buildServer.agent.FlowGenerator;
import jetbrains.buildServer.agent.FlowLogger;
import jetbrains.buildServer.messages.DefaultMessagesInfo;
import jetbrains.buildServer.util.PropertiesUtil;
import net.obive.teamcity.jarrunner.JarRunnerConstants;
import org.apache.commons.text.StringSubstitutor;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileReader;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

class JarRunnerBuildTask implements Runnable {
	private static final String PREPARING_TO_RUN_JAVA_CLASS = "Preparing";
	private final JarRunnerBuildProcess buildProcess;
	private final AgentRunningBuild agentRunningBuild;
	private final BuildRunnerContext context;
	private final BuildProgressLogger buildLogger;
	private final String myFlowId;

	public JarRunnerBuildTask(JarRunnerBuildProcess buildProcess, AgentRunningBuild agentRunningBuild, BuildRunnerContext context) {
		this.buildProcess = buildProcess;
		this.agentRunningBuild = agentRunningBuild;
		this.context = context;
		this.buildLogger = agentRunningBuild.getBuildLogger();
		this.myFlowId = FlowGenerator.generateNewFlow();
	}
	public void run() {

		boolean problems = false;
		Object instance = null;
		String mainClass = "<unknown>";
		AtomicReference<BuildFinishedStatus> finishedStatus = new AtomicReference<>();
		FlowLogger flowLogger = buildLogger.getFlowLogger(myFlowId);

		try {
			flowLogger.startFlow();
			buildLogger.activityStarted(PREPARING_TO_RUN_JAVA_CLASS, DefaultMessagesInfo.BLOCK_TYPE_BUILD_STEP);
			buildLogger.message("Getting ready to run Java Class");

			String checkoutDirectory = context.getBuild().getCheckoutDirectory().getCanonicalPath();
			String workingDirectory = context.getWorkingDirectory().getCanonicalPath();

			buildLogger.message("Current working directory is " + workingDirectory);
			buildLogger.message("Current checkout directory is " + checkoutDirectory);

			final Map<String, String> runParams = new HashMap<>(context.getRunnerParameters()); // all server-ui options

			mainClass = runParams.get(JarRunnerConstants.SERVER_UI_MAIN_CLASS);
			final String classPath = runParams.get(JarRunnerConstants.SERVER_UI_JVM_CLASS_PATH);
			final String envFilePath = runParams.get(JarRunnerConstants.SERVER_UI_ENV_FILE);
			final ArrayList<String> classPaths = new ArrayList<>();


			// Load env file
			if (envFilePath != null && envFilePath.trim().length() > 0) {
				final File envFileFile = Paths.get(workingDirectory, envFilePath).toFile();

				buildLogger.message("Loading env file and performing substitutions: " + envFilePath + " > " + envFileFile.getAbsolutePath());
				Properties prop = new Properties();
				prop.load(new FileReader(envFileFile));

				StringSubstitutor sub = getStringSubstitutor(prop);

				sub.setEnableSubstitutionInVariables(true);
				try {

					for (Object k : prop.keySet()) {
						String replace = sub.replace(prop.get(k));
						prop.setProperty((String) k, replace);
					}
				} catch (Exception ex) {
					buildLogger.exception(ex);
				}
				final String config = runParams.get(JarRunnerConstants.SERVER_UI_CONFIG);
				context.addRunnerParameter(JarRunnerConstants.SERVER_UI_CONFIG_REPLACED, sub.replace(config));
			}


			ArrayList<URL> classPathUrls = new ArrayList<>();

			if (!PropertiesUtil.isEmptyOrNull(classPath)) {
				classPaths.addAll(Arrays.asList(classPath.split("\\R+")));
			}

			buildLogger.message("Loading " + classPaths.size() + " classpaths");
			for (String path : classPaths) {
				File file = Paths.get(workingDirectory, path).toFile();


				if (path.endsWith("/*")) {
					path = path.substring(0, path.length() - 2);
					file = Paths.get(workingDirectory, path).toFile();
					if (file.isDirectory() && file.exists()) {
						for (File f : file.listFiles()) {
							problems = problems || addClasspaths(classPathUrls, file.getPath(), f);
						}
					} else {
						buildLogger.warning("Path is not a directory or doesn't exist: " + path);
					}
				} else {
					problems = problems || addClasspaths(classPathUrls, path, file);
				}
			}

			Class<?> aClass;
			aClass = loadClass(mainClass, classPathUrls);
			if (aClass != null) {
				instance = getInstance(aClass, finishedStatus);
			}
		} catch (Throwable ex) {
			buildLogger.exception(ex);
		} finally {
			buildLogger.activityFinished(PREPARING_TO_RUN_JAVA_CLASS, DefaultMessagesInfo.BLOCK_TYPE_BUILD_STEP);
		}
		if (instance != null) {
			String simpleName = instance.getClass().getSimpleName();
			buildLogger.message("Starting " + simpleName);
			buildLogger.activityStarted(simpleName, DefaultMessagesInfo.BLOCK_TYPE_BUILD_STEP);

			try {
				((Runnable) instance).run();
				buildProcess.setFinishedStatus(finishedStatus.get() != null ? finishedStatus.get() :
											   problems ? BuildFinishedStatus.FINISHED_WITH_PROBLEMS : BuildFinishedStatus.FINISHED_SUCCESS);
				buildLogger.message("Runner complete");
			} catch (Throwable ex) {
				buildLogger.error("Uncaught exception while running " + simpleName);
				buildLogger.exception(ex);
				buildProcess.setFinishedStatus(BuildFinishedStatus.FINISHED_FAILED);
			} finally {
				buildLogger.activityFinished(simpleName, DefaultMessagesInfo.BLOCK_TYPE_BUILD_STEP);
				try {
					((URLClassLoader) instance.getClass().getClassLoader()).close();
				} catch (Throwable ex) {
					buildLogger.warning("Unable to unload temporary classloader: " + ex.toString());
				}
			}
		} else {
			buildLogger.error("JarRunner could not instantiate an instance to run.");
		}
		flowLogger.disposeFlow();
		buildProcess.setFinished(true);
	}
	private boolean addClasspaths(ArrayList<URL> classPathUrls, String path, File file) {
		boolean problems = false;
		try {
			buildLogger.message("Loaded classpath: " + file.getAbsolutePath());
			if (!file.exists()) {
				problems = true;
				buildLogger.warning("Classpath doesn't exist, will try to use it anyway: " + path + "(" + file.getAbsolutePath() + ")");
			}
			classPathUrls.add(file.toURI().toURL());
		} catch (MalformedURLException e) {
			problems = true;
			buildLogger.error("Failed to convert file to URL: " + file.toString());
		}
		return problems;
	}
	@NotNull
	private StringSubstitutor getStringSubstitutor(Properties prop) {
		return new StringSubstitutor(s -> {
			Object setting = prop.get(s);
			return setting == null ? null : setting.toString();
		}, "{", "}", '$');
	}
	private Class<?> loadClass(String mainClass, ArrayList<URL> classPathUrls) {
		Class<?> result = null;
		URLClassLoader child = null;
		buildLogger.message("Trying to load main class '" + mainClass + "'");

		try {
			child = new URLClassLoader(classPathUrls.toArray(new URL[0]), this.getClass().getClassLoader());
			result = Class.forName(mainClass, true, child);
			buildLogger.message("Loaded main class");
		} catch (ClassNotFoundException e) {
			buildLogger.error("Main class not found.");
			buildLogger.exception(e);
			buildLogger.logBuildProblem(BuildProblemData.createBuildProblem("net.obive.teamcity.jarrunner.mainClassNotFound", "MainClassNotFound", "The main class could not be found", mainClass));
		}
		return result;
	}
	private Object getInstance(Class<?> aClass, AtomicReference<BuildFinishedStatus> finishedStatus) {
		Object result = null;
		String mainClass = aClass.getCanonicalName();

		try {
			Constructor<?> constructor = aClass.getConstructor(AgentRunningBuild.class, BuildRunnerContext.class, Consumer.class);
			result = constructor.newInstance(agentRunningBuild, context, (Consumer<BuildFinishedStatus>) finishedStatus::set);
		} catch (NoSuchMethodException e) {
			buildLogger.message("No constructor that takes (AgentRunningBuild.class, BuildRunnerContext.class, Consumer.class), trying default constructor");
			try {
				Constructor<?> constructor = aClass.getConstructor();
				result = constructor.newInstance();
			} catch (NoSuchMethodException e1) {
				buildLogger.error("No constructor found");
				e1.printStackTrace();
			} catch (InstantiationException | IllegalAccessException | InvocationTargetException e1) {
				buildLogger.error("Could not call default constructor");
				buildLogger.exception(e1);
				buildLogger.logBuildProblem(BuildProblemData.createBuildProblem("net.obive.teamcity.jarrunner.couldNotInstantiate", "CouldNotInstantiate", "Could not instantiate instance of " + mainClass));
			}
		} catch (IllegalAccessException | InstantiationException | InvocationTargetException e) {
			buildLogger.error("Failed to call constructor");
			buildLogger.exception(e);
			buildLogger.logBuildProblem(BuildProblemData.createBuildProblem("net.obive.teamcity.jarrunner.couldNotInstantiate", "CouldNotInstantiate", "Could not instantiate instance of " + mainClass));
		}

		if (result == null) {
			buildLogger.error("Could not instantiate new instance of " + mainClass);
		} else if (!(result instanceof Runnable)) {
			result = null;
			buildLogger.buildFailureDescription("Main class not Runnable");
		}
		return result;
	}
}
