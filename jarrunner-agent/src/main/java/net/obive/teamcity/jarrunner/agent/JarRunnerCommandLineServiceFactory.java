package net.obive.teamcity.jarrunner.agent;

import jetbrains.buildServer.agent.AgentBuildRunnerInfo;
import jetbrains.buildServer.agent.BuildAgentConfiguration;
import jetbrains.buildServer.agent.runner.CommandLineBuildService;
import jetbrains.buildServer.agent.runner.CommandLineBuildServiceFactory;
import net.obive.teamcity.jarrunner.JarRunnerConstants;
import org.jetbrains.annotations.NotNull;

public class JarRunnerCommandLineServiceFactory implements CommandLineBuildServiceFactory {
	@NotNull
	public CommandLineBuildService createService() {
		return new JarRunnerBuildService();
	}

	@NotNull
	public AgentBuildRunnerInfo getBuildRunnerInfo() {
		return new AgentBuildRunnerInfo() {
			@NotNull
			public String getType() {
				return JarRunnerConstants.RUNNER_TYPE;
			}

			public boolean canRun(@NotNull final BuildAgentConfiguration buildAgentConfiguration) {
				return true;
			}
		};
	}
}
