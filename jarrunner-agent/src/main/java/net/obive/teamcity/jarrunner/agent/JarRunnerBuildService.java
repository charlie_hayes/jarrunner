package net.obive.teamcity.jarrunner.agent;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import jetbrains.buildServer.RunBuildException;
import jetbrains.buildServer.agent.BuildRunnerContext;
import jetbrains.buildServer.agent.runner.*;
import jetbrains.buildServer.runner.BuildFileRunnerUtil;
import jetbrains.buildServer.util.PropertiesUtil;
import jetbrains.buildServer.util.StringUtil;
import net.obive.teamcity.jarrunner.JarRunnerConstants;
import org.jetbrains.annotations.NotNull;


public class JarRunnerBuildService extends BuildServiceAdapter {
	private final Set<File> filesToDelete = new HashSet<>();



	@NotNull
	@Override
	public ProgramCommandLine makeProgramCommandLine() throws RunBuildException {
		try {

			final List<String> arguments = new ArrayList<>();
			final BuildRunnerContext context = getRunnerContext();

			String checkoutDirectory = context.getBuild().getCheckoutDirectory().getCanonicalPath();
			String workingDirectory = context.getWorkingDirectory().getCanonicalPath();

			// runParams - all server-ui options
			// buildParams - system properties (system.*), environment vars (env.*)
			final Map<String, String> runParams = new HashMap<>(context.getRunnerParameters());
			final Map<String, String> buildParams = context.getBuildParameters().getAllParameters();
			final Map<String, String> env = context.getBuildParameters().getEnvironmentVariables();

			final File jarFile = BuildFileRunnerUtil.getBuildFile(runParams);

			if (!jarFile.exists()) {
				throw new RunBuildException("Must specify JAR file");
			}

			String jarFilePath = jarFile.getAbsolutePath();

			final String mainClass = runParams.get(JarRunnerConstants.SERVER_UI_MAIN_CLASS);
			final String classPath = runParams.get(JarRunnerConstants.SERVER_UI_JVM_CLASS_PATH);
//			final String vmOptions = runParams.get(JarRunnerConstants.SERVER_UI_JVM_VMOPTIONS);
			final ArrayList<String> classPaths = new ArrayList<>();


			if (PropertiesUtil.isEmptyOrNull(classPath)) {
				classPaths.addAll(Arrays.asList(classPath.split("\\R+")));
			}
//			if (PropertiesUtil.isEmptyOrNull(vmOptions)) {
//				// Save the vm options to a temp file and pass to java
//				File vmOptionsFile = File.createTempFile("jarrunner", "vmoptions");
//				new PrintWriter(vmOptionsFile).print(vmOptions);
//				filesToDelete.add(vmOptionsFile);
//				arguments.add("-Xoptionsfile");
//				arguments.add(vmOptionsFile.getAbsolutePath());
//			}

			// Add classpath
			classPaths.add(jarFilePath);
			arguments.add("-cp");
			arguments.add(String.join(String.valueOf(File.pathSeparatorChar), classPaths));

			if (PropertiesUtil.isEmptyOrNull(mainClass)) {
				arguments.add("-jar");
				arguments.add(jarFilePath);
			} else {
				arguments.add(mainClass);
			}

			Map<String, String> nonEmptyEnvVars = env
					.entrySet()
					.stream()
					.filter(e -> !StringUtil.isEmptyOrSpaces(e.getValue()))
					.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

			return new SimpleProgramCommandLine(nonEmptyEnvVars,
												getWorkingDirectory().getAbsolutePath(),
												"java",
												arguments);
		} catch (IOException e) {
			throw new RunBuildException(e.getMessage(), e);
		}
	}


	@Override
	public void afterProcessFinished() {
		// Remove tmp files
		for (File file : filesToDelete) {
			jetbrains.buildServer.util.FileUtil.delete(file);
		}
		filesToDelete.clear();
	}

	@NotNull
	@Override
	public List<ProcessListener> getListeners() {
		return Collections.singletonList(new LoggingProcessListener(getLogger()) {
			@Override
			public void onErrorOutput(@NotNull final String text) {
				if (text.trim().startsWith("org.jruby.exceptions.RaiseException: (SystemExit) exit")) {
					super.onStandardOutput(text);
				} else {
					super.onErrorOutput(text);
				}
			}
		});
	}
}
