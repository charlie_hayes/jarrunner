package net.obive.teamcity.jarrunner.agent;

import jetbrains.buildServer.RunBuildException;
import jetbrains.buildServer.agent.AgentBuildRunner;
import jetbrains.buildServer.agent.AgentBuildRunnerInfo;
import jetbrains.buildServer.agent.AgentRunningBuild;
import jetbrains.buildServer.agent.BuildAgentConfiguration;
import jetbrains.buildServer.agent.BuildProcess;
import jetbrains.buildServer.agent.BuildRunnerContext;
import net.obive.teamcity.jarrunner.JarRunnerConstants;
import org.jetbrains.annotations.NotNull;

public class JarRunnerAgentExtension implements AgentBuildRunner {

	public JarRunnerAgentExtension() {

	}

	public static final AgentBuildRunnerInfo AGENT_BUILD_RUNNER_INFO = new AgentBuildRunnerInfo() {
		@NotNull
		public String getType() {
			return JarRunnerConstants.RUNNER_TYPE;
		}
		public boolean canRun(@NotNull BuildAgentConfiguration buildAgentConfiguration) {
			// Can always run since we only depend on the JRE which is already running TeamCity
			return true;
		}
	};
	@NotNull
	public BuildProcess createBuildProcess(@NotNull AgentRunningBuild agentRunningBuild, @NotNull BuildRunnerContext context) throws
																															  RunBuildException {
		return new JarRunnerBuildProcess(agentRunningBuild, context);
	}
	@NotNull
	public AgentBuildRunnerInfo getRunnerInfo() {
		return AGENT_BUILD_RUNNER_INFO;
	}
}

